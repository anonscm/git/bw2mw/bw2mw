#!/bin/mksh
# $Id$
#-
# Copyright © 2009
#	Thorsten Glaser <t.glaser@tarent.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

if [[ -z $1 || ! -s $1 ]]; then
	print Make sure ASCII 1Ch, 1Dh, 1Eh do not appear in the CSV file!
	print Export options for phpMyAdmin are:
	print '\tCSV, Fields terminated by "`fs!", Fields enclosed by "",'
	print '\tFields escaped by "", Lines terminated by "`rs!",'
	print '\tReplace NULL by "", [x] Put field names in the first row'
	print "Syntax: $0 foo.csv >foo.txt"
	exit 1
fi

set -A encfix
set -A ansicp
i=128
while (( i < 256 )); do
	# UCS-2 → latin1
	(( encfix[i] = 0xEF80 | i ))
	# latin1 → latin1 (passthrough)
	(( encfix[0xEF80 | i] = 0xEF80 | i ))
	# latin1 → UCS-2
	(( ansicp[0xEF80 | i] = i < 0xA0 ? 0xFFFD : i ))
	let i++
done
# UCS-2 → cp1252
encfix[0x20AC]=0xEF80
encfix[0x201A]=0xEF82
encfix[0x0192]=0xEF83
encfix[0x201E]=0xEF84
encfix[0x2026]=0xEF85
encfix[0x2020]=0xEF86
encfix[0x2021]=0xEF87
encfix[0x02C6]=0xEF88
encfix[0x2030]=0xEF89
encfix[0x0160]=0xEF8A
encfix[0x2039]=0xEF8B
encfix[0x0152]=0xEF8C
encfix[0x017D]=0xEF8E
encfix[0x2018]=0xEF91
encfix[0x2019]=0xEF92
encfix[0x201C]=0xEF93
encfix[0x201D]=0xEF94
encfix[0x2022]=0xEF95
encfix[0x2013]=0xEF96
encfix[0x2014]=0xEF97
encfix[0x02DC]=0xEF98
encfix[0x2122]=0xEF99
encfix[0x0161]=0xEF9A
encfix[0x203A]=0xEF9B
encfix[0x0153]=0xEF9C
encfix[0x017E]=0xEF9E
encfix[0x0178]=0xEF9F
# cp1252 → UCS-2
ansicp[0xEF80]=0x20AC
ansicp[0xEF82]=0x201A
ansicp[0xEF83]=0x0192
ansicp[0xEF84]=0x201E
ansicp[0xEF85]=0x2026
ansicp[0xEF86]=0x2020
ansicp[0xEF87]=0x2021
ansicp[0xEF88]=0x02C6
ansicp[0xEF89]=0x2030
ansicp[0xEF8A]=0x0160
ansicp[0xEF8B]=0x2039
ansicp[0xEF8C]=0x0152
ansicp[0xEF8E]=0x017D
ansicp[0xEF91]=0x2018
ansicp[0xEF92]=0x2019
ansicp[0xEF93]=0x201C
ansicp[0xEF94]=0x201D
ansicp[0xEF95]=0x2022
ansicp[0xEF96]=0x2013
ansicp[0xEF97]=0x2014
ansicp[0xEF98]=0x02DC
ansicp[0xEF99]=0x2122
ansicp[0xEF9A]=0x0161
ansicp[0xEF9B]=0x203A
ansicp[0xEF9C]=0x0153
ansicp[0xEF9E]=0x017E
ansicp[0xEF9F]=0x0178

function fixencoding {
	typeset line ostr u=$-
	typeset -i1 wc
	set -U

	while IFS= read -r line; do
		# convert Swedish double-UTF8 into UTF-8 proper
		# but trash potential latin1/cp1252
		ostr=
		while [[ -n $line ]]; do
			wc=1#${line::1}
			line=${line:1}
			(( wc < 128 )) || \
			    (( wc = encfix[wc] ? encfix[wc] : 0xFFFD ))
			ostr=$ostr${wc#1#}
		done
		# ostr now contains "fixed-up" UTF-8 possibly mixed with latin1
		line=$ostr
		# convert trashed latin1/cp1252 back into UTF-8
		# after this, ostr contains ONLY valid UTF-8
		ostr=
		while [[ -n $line ]]; do
			wc=1#${line::1}
			line=${line:1}
			(( wc = ((wc & 0xFF80) == 0xEF80) ? ansicp[wc] : wc ))
			ostr=$ostr${wc#1#}
		done
		print -r -- "$ostr"
	done

	[[ $u = *U* ]] || set +U
	:
}

set -A sedexp
addsedexp() {
	sedexp[${#sedexp[*]}]=-e
	sedexp[${#sedexp[*]}]=$1
}

# convert CR-LF (ASCII newline) to LF (UNIX newline)
addsedexp 's,$,,g'
# convert phpMyAdmin field separator to ASCII field separator
addsedexp 's,`fs!,,g'
# convert phpMyAdmin record separator to ASCII record separator
addsedexp 's,`rs!,,g'
# fixup phpMyAdmin first line buggy output
addsedexp 's,`fs,,g'

# 1+2)	read the file and append a UNIX newline, for the read builtin to work
# 3)	convert MySQL Swedish encoded doubly-converted UTF-8 to proper UTF-8
# 4)	apply the aforementioned regexps
# 5a)	translate in-field newlines to ASCII group separator
# 5b)	translate ASCII record separator to UNIX newline (for the next script)
# 6)	remove the artefact from (1) in the last line
print | cat "$1" - | fixencoding | sed "${sedexp[@]}" | tr '\n' '\n' | \
    sed -e '$d'
